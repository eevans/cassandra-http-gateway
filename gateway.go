/*
 * Copyright 2022 Eric Evans <eevans@wikimedia.org> and Wikimedia Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gateway

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"
	"github.com/lann/builder"
	"github.com/prometheus/client_golang/prometheus"
	"schneider.vip/problem"
)

// Relation types correspond to CQL statement relations.
type Relation string

const (
	EQ  Relation = "="
	GT  Relation = ">"
	LT  Relation = "<"
	GTE Relation = ">="
	LTE Relation = "<="
)

// Predicate represents a predicate in the WHERE clause of a CQL SELECT.
type Predicate struct {
	Column   string
	Relation Relation
	Value    interface{}
}

// Field correponds to a field in the projection of a CQL SELECT statement.
type Field struct {
	Column string
	CastTo string
	Alias  string
}

// SelectStatement represents a CQL SELECT statement
type SelectStatement struct {
	Keyspace   string
	Table      string
	Fields     []Field
	Predicates []Predicate

	Session      *gocql.Session
	CounterVec   *prometheus.CounterVec
	HistogramVec *prometheus.HistogramVec
	Logger       *log.Logger
	ctx          context.Context
}

// String returns the CQL query string for a SelectStatement
func (s *SelectStatement) String() string {
	var b strings.Builder

	printf := func(format string, args ...interface{}) {
		fmt.Fprintf(&b, format, args...)
	}

	printf("SELECT JSON ")

	if len(s.Fields) > 0 {
		for i, f := range s.Fields {
			if f.CastTo != "" {
				printf("cast(%s AS %s)", f.Column, f.CastTo)
			} else {
				printf("%s", f.Column)
			}
			if f.Alias != "" {
				printf(" AS %s", f.Alias)
			}
			if i < (len(s.Fields) - 1) {
				printf(", ")
			}
		}
		printf(" ")
	} else {
		printf("* ")
	}

	printf("FROM %s.%s ", s.Keyspace, s.Table)

	if len(s.Predicates) > 0 {
		printf("WHERE ")
		for i, p := range s.Predicates {
			printf("%s %s ?", p.Column, p.Relation)
			if i < (len(s.Predicates) - 1) {
				printf(" AND ")
			}
		}
	}

	return b.String()
}

// Handle processes an HTTP request, querying the database for results, and returning them as a JSON-formatted response.
func (s *SelectStatement) Handle(w http.ResponseWriter, request *http.Request) {
	var err error
	var query = s.String()
	var res string
	var response = newStatusObserver(w)
	var start time.Time = time.Now()

	// Update Prometheus metrics on the way out
	defer func() {
		if s.CounterVec != nil {
			s.CounterVec.WithLabelValues(strconv.Itoa(response.status), request.Method).Inc()
		}

		if s.HistogramVec != nil {
			s.HistogramVec.WithLabelValues(strconv.Itoa(response.status), request.Method).Observe(time.Since(start).Seconds())
		}
	}()

	s.Logger.Request(request).Log(log.DEBUG, "Executing CQL: %s", query)

	if err = s.Session.Query(query, s.values()...).WithContext(s.ctx).Scan(&res); err != nil {
		if err == gocql.ErrNotFound {
			s.Logger.Request(request).Log(log.DEBUG, "Query returned no matching results")
			problem.New(
				problem.Status(http.StatusNotFound),
				problem.Title("No matching records found"),
				problem.Detail("There were no matching records found, check your query parameters and try again."),
			).WriteTo(response)
		} else {
			s.Logger.Request(request).Log(log.ERROR, "%s", err)
			problem.New(
				problem.Status(http.StatusInternalServerError),
				problem.Title("Cassandra query error"),
				problem.Detail(fmt.Sprintf("An unknown error occurred, contact the administrator(s) (%s)", err)),
			).WriteTo(response)
		}
		w.Header().Set("Content-Type", "application/problem+json")
		return
	}

	response.Header().Set("Content-Type", "application/json")
	response.Write([]byte(res))
}

func (s *SelectStatement) values() []interface{} {
	var values = make([]interface{}, 0)
	for _, v := range s.Predicates {
		values = append(values, v.Value)
	}
	return values
}

// statusObserver wraps a ResponseWriter in order to track the status code for later use.
type statusObserver struct {
	http.ResponseWriter
	status int
}

// WriteHeader writes an HTTP response status code to the ResponseWriter and status observer.
func (r *statusObserver) WriteHeader(code int) {
	r.status = code
	r.ResponseWriter.WriteHeader(code)
}

// Returns a new statusObserver with a default status
func newStatusObserver(w http.ResponseWriter) *statusObserver {
	return &statusObserver{w, 200}
}

// Builders ~~~

// FieldBuilderType is a fluent DSL for constructing Fields
type FieldBuilderType builder.Builder

// Column assigns the column name attribute of a Field, and returns the builder.
func (b FieldBuilderType) Column(name string) FieldBuilderType {
	return builder.Set(b, "Column", name).(FieldBuilderType)
}

// CastTo assigns the type to cast a Field value to, and returns the builder.
func (b FieldBuilderType) CastTo(kind string) FieldBuilderType {
	return builder.Set(b, "CastTo", kind).(FieldBuilderType)
}

// Alias assigns an alias for the Field, and returns the builder.
func (b FieldBuilderType) Alias(label string) FieldBuilderType {
	return builder.Set(b, "Alias", label).(FieldBuilderType)
}

// Get constructs and returns a Field object
func (b FieldBuilderType) Get() Field {
	// FIXME: Validate resulting struct before returning (*must* have Column!)
	return builder.GetStruct(b).(Field)
}

// SelectBuilderType is a fluent DSL for constructing SelectStatements
type SelectBuilderType builder.Builder

func (b SelectBuilderType) keyspace(name string) SelectBuilderType {
	return builder.Set(b, "Keyspace", name).(SelectBuilderType)
}

func (b SelectBuilderType) table(name string) SelectBuilderType {
	return builder.Set(b, "Table", name).(SelectBuilderType)
}

// Select adds zero or more fields to the projection of the SELECT statement being constructed.  If no fields are
// added (or if Select() is never invoked), then the projection will contain '*'.
func (b SelectBuilderType) Select(fields ...Field) SelectBuilderType {
	return builder.Extend(b, "Fields", fields).(SelectBuilderType)
}

// From assigns the keyspace and table name of the SELECT  statement under construction.
func (b SelectBuilderType) From(keyspace, table string) SelectBuilderType {
	return b.keyspace(keyspace).table(table)
}

// Bind creates predicates with an EQ ('=') relation for each of the parameters in the supplied httprouter.Params.
func (b SelectBuilderType) Bind(ps httprouter.Params) SelectBuilderType {
	var bb SelectBuilderType = b
	for _, p := range ps {
		bb = bb.Where(p.Key, EQ, p.Value)
	}
	return bb
}

// Where adds a predicate to the SELECT statement under construction.
func (b SelectBuilderType) Where(column string, relation Relation, value interface{}) SelectBuilderType {
	return builder.Append(b, "Predicates", Predicate{Column: column, Relation: relation, Value: value}).(SelectBuilderType)
}

// Session assigns a GoCQL Session object to the statement under construction.  The Session supplied will be used
// to execute CQL queries against.
func (b SelectBuilderType) Session(session *gocql.Session) SelectBuilderType {
	return builder.Set(b, "Session", session).(SelectBuilderType)
}

// CounterVec assigns a Prometheus CounterVec object to the statement under construction. The object supplied will
// be used to count HTTP requests proccessed by Handle().
func (b SelectBuilderType) CounterVec(counter *prometheus.CounterVec) SelectBuilderType {
	return builder.Set(b, "CounterVec", counter).(SelectBuilderType)
}

// HistogramVec assigns a Prometheus HistogramVec object to the statement under construction. The object supplied will
// be used to create a latency histogram of HTTP requests processed by Handle().
func (b SelectBuilderType) HistogramVec(histogram *prometheus.HistogramVec) SelectBuilderType {
	return builder.Set(b, "HistogramVec", histogram).(SelectBuilderType)
}

// Logger assigns a logger object to the statement under construction.  The supplied object will be used to output
// logging information from HTTP requests processed by Handle().
func (b SelectBuilderType) Logger(logger *log.Logger) SelectBuilderType {
	return builder.Set(b, "Logger", logger).(SelectBuilderType)
}

// Build constructs and returns a SelectStatement.
func (b SelectBuilderType) Build() *SelectStatement {
	// FIXME: Validate resulting struct before returning (*must* have a keyspace and table name!)
	var s SelectStatement = builder.GetStruct(b).(SelectStatement)
	s.ctx = context.Background()
	return &s
}

// Column is convience function that returns a new Field builder with the supplied column name assigned.
func Column(name string) FieldBuilderType {
	return FieldBuilder.Column(name)
}

var (
	SelectBuilder = builder.Register(SelectBuilderType{}, SelectStatement{}).(SelectBuilderType)
	FieldBuilder  = builder.Register(FieldBuilderType{}, Field{}).(FieldBuilderType)
)
