# cassandra-http-gateway

An HTTP data gateway framework for Apache Cassandra

See also: [T294468](https://phabricator.wikimedia.org/T294468)

## Usage

```golang
package main

import (
    "fmt"
    "os"

    "github.com/gocql/gocql"
    "github.com/julienschmidt/httprouter"
)

func main() {
    var cluster *gocql.ClusterConfig
    var err error
    var session *gocql.Session

    cluster = gocql.NewCluster("localhost")
    cluster.Consistency = gocql.One

    if session, err = cluster.CreateSession(); err != nil {
        fmt.Fprintln(os.Stderr, err)
        os.Exit(1)
    }

    var router = new httprouter.New()
    router.GET("/music/:artist/:album", func(w http ResponseWriter, r *http.Request, ps httprouter.Params) {
        SelectBuilder.
            From("music", "tracks").
            Bind(ps).Session(session).
            Build().
            Handle(w)
    })
}
```
